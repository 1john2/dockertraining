#!/usr/bin/env bash

#=============== HELPER FUNCTIONS  ===============
function _eval_param {
# Evaluate parameter value.
# If there is parameter specific value for system and role, use it
# Otherwise default
# $1 - system_name = ECS|BSP|SharedCloud|etc
# $2 - role = horizon|CIC|fuel|CLI
# $3 - param_name = host|port|username|password
# return value
  param_name="${1}"
  role_name="${2}"
  prefix_system_name="${3}"
  param="$(eval echo \$${prefix_system_name}${role_name}_${param_name})"
  if [ -z "${param}" ]; then
    param="$(eval echo \$default_${role_name}_${param_name})"
  fi
  echo "${param}"
}

function ssh_gen_alias {
# Generate ssh alias for one host.
# Input:
# $1 - role_name - horizon|master|fuel|kvm..
# $2 - system_name - if role is part of system, specify here - BSP05|SharedCloud...
# returns: export alias for ssh and update no_proxy
  role_name="${1}"
  system_name="${2}"
  if [ -z "${system_name}" ]; then
    prefix_system_name=""
    suffix_system_name=""
  else
    prefix_system_name="${system_name}_"
    suffix_system_name="_${system_name}"
  fi

  if [[ ${role_name} = "master"* ]]; then
  # handle masters username - it's the same
    updated_role_name='master'
  else
    updated_role_name=${role_name}
  fi

  user=$(_eval_param "user" "${updated_role_name}" "${prefix_system_name}")
  pw=$(_eval_param "pw" "${updated_role_name}" "${prefix_system_name}")
  host=$(_eval_param "host" "${role_name}" "${prefix_system_name}")
  port=$(_eval_param "port" "${updated_role_name}" "${prefix_system_name}")

  # =============== create ssh alias 
  alias "s_${role_name}${suffix_system_name}"="sshpass -p '${pw}' ssh -p ${port} ${user}@${host}"
  export no_proxy="${no_proxy},${host}"
} 

function ssh_cee_alias_generator {
  # =============== helper vars
  system_name=$1
  ssh_gen_alias 'horizon' ${system_name}
  ssh_gen_alias 'fuel' ${system_name}
  ssh_gen_alias 'cli' ${system_name}
  ssh_gen_alias 'master1' ${system_name}
  # =============== create ssh alias for master[123]
  for iter in 1 2 3;do 
    ssh_gen_alias "master${iter}" ${system_name}
  done
}

function ssh_auth_forwarding {
  USER_HOME="$(echo ~)"
 # enable ssh auth forwarding in screen
  if [[ -S "$SSH_AUTH_SOCK" && ! -h "$SSH_AUTH_SOCK" ]]; then
    ln -sf "$SSH_AUTH_SOCK" "${USER_HOME}/.ssh/ssh_auth_sock"
  fi
  export SSH_AUTH_SOCK="${USER_HOME}/.ssh/ssh_auth_sock"
}

#=============== BASH GENERIC STUFF ===============
set -o vi #enable vi editing style
export SHELL='/bin/bash' #/bin/tcsh is not overwritten when bash starts
export HISTSIZE='' #unlimited hist size
export HISTFILESIZE='' #unlimited hist size
export HISTCONTROL=ignoreboth:erasedups # ignore dups in history
export LOCKPRG='/bin/true' #disable screen session lock
export LANG='en_US.utf8'
export PAGER='less -R'

#=============== enable ssh auth agent forwarding =============== 
ssh_auth_forwarding

#=============== APPS ALIASES ===============
alias gs='git status'
alias ga='git add'
alias gd='git diff'
alias gc='git commit -sv'
alias gdiff='git diff'
alias gca='git commit --amend -sv'
alias gl='git log --pretty --graph --all'
alias ll='ls -l'
alias la='ls -al'
alias dps='docker ps -a'
alias di='docker images'
alias dinspect='docker inspect'
alias de='docker exec -it'
alias drm='docker rm'
alias drmi='docker rmi'
alias drm_all='for i in $(docker ps -aq | grep -v $(docker ps -a | grep jumphost | cut -d" " -f1 ) \
                                        | grep -v $(docker ps -a | grep registry | cut -d" " -f1 )); \
                  do docker stop $i; docker rm --force $i;done' #'for i in $(docker ps -aq);do docker stop $i; docker rm --force $i;done'
alias drmi_all='for i in $(docker images -q);do docker rmi $i;done'

#=============== APPS as variables ===============

#=============== CREDENTIALS & SSH Aliases ===============
#=============== README ===============
# before you will use generated aliases please update your ~/.ssh/config with section below
# otherwise you will not be able to login. Connection will stuck on HIDDEN check host key (...) yes/no
#Host *
#  StrictHostKeyChecking no

#=============== default values ===============
default_kvm_user='wrong'
default_kvm_pw='wrong'
default_kvm_port='22222222'

default_horizon_user='horizonadm'
default_horizon_port=22
default_horizon_pw='wrong'

default_master_user='adm'
default_master_pw=''
default_master_port=22

default_fuel_user='root'
default_fuel_port=22

default_cli_user='test'
default_cli_port=22

export no_proxy="localhost,127.0.0.1"

#=============== KVM host - SB6_2 blade 2
kvm_user='root'
kvm_pw='rootrootroot'
kvm_host='10.35.21.40'
kvm_port='22'
ssh_gen_alias kvm

#=============== BSP05 ===============
testsystem_horizon_pw=''
testsystem_master_pw=''
testsystem_fuel_pw=''
testsystem_cli_pw=''
export testsystem_horizon_host=''
export testsystem_fuel_host=''
export testsystem_master1_host=''
export testsystem_master2_host=''
export testsystem_master3_host=''
export testsystem_cli_host=''
export testsystem_cli_port=''
ssh_cee_alias_generator testsystem

#=============== import colors codes and create pretty PS1  ===============
# Various variables you might want for your PS1 prompt instead
Time12h="\T"
Time12a="\@"
PathShort="\w"
PathFull="\W"
NewLine="\n"
Jobs="\j"
Username="\u"
Hostname="\h"

if ! [ -z ${MODE+x} ] && [ ${MODE} == "PRESENTATION" ] && [ -f ~/.bash_nocolors ]; then
  source ~/.bash_nocolors
  customps1=true
elif [ -f ~/.bash_nocolors ]; then
  source ~/.bash_colors
  customps1=true
fi
	#  Customize BASH PS1 prompt to show current GIT repository and branch.
	#  by Mike Stewart - http://MediaDoneRight.com
	
	#  SETUP CONSTANTS
	#  Bunch-o-predefined colors.  Makes reading code easier than escape sequences.
	#  I don't remember where I found this.  o_O
	# This PS1 snippet was adopted from code for MAC/BSD I saw from: http://allancraig.net/index.php?option=com_content&view=article&id=108:ps1-export-command-for-git&catid=45:general&Itemid=96
	# I tweaked it to work on UBUNTU 11.04 & 11.10 plus made it mo' better
	export PS1=$BPurple$Time12h$Color_Off-$BCyan$Username$Color_Off@$BGreen$Hostname-$Yellow$PathShort$Color_Off:
	#export PS1="$BPurple\t\[\033[m\]-$BCyan\u$Color_Off@$BGreen\h:'$(git branch &>/dev/null;\
# if [ ${customps1} ]; then
#	export PS1=$BPurple$Time12h$Color_Off-$BCyan$Username$Color_Off@$BGreen$Hostname: #'$(if [ -d "./.git" ]; then \
#    git branch &>/dev/null;\
#		if [ $? -eq 0 ]; then \
#		  echo "$(echo `git status` | grep "nothing to commit" > /dev/null 2>&1; \
#		  if [ "$?" -eq "0" ]; then \
#		    # @4 - Clean repository - nothing to commit
#		    echo "'$Green'"$(__git_ps1 " (%s)"); \
#		  else \
#		    # @5 - Changes to working tree
#		    echo "'$IRed'"$(__git_ps1 " {%s}"); \
#		  fi) '$BYellow$PathShort$Color_Off'\$ "; \
#    fi; \
#		else \
#		  # @2 - Prompt when not in GIT repo
#		  echo " '$Yellow$PathShort$Color_Off'\$ "; \
#		fi)'
#fi
