#!/usr/bin/env bash
set -x

### README
#
# usefull aliases are enabled in .bashrc:
# alias dps='docker ps -a'
# alias di='docker images'
# alias dinspect='docker inspect'
# alias de='docker exec -it'
# alias drm='docker rm'
# alias drmi='docker rmi'
# alias drm_all='for i in $(docker ps -aq | grep -v $(docker ps -a | grep jumphost | cut -d" " -f1 ) \
#                                         | grep -v $(docker ps -a | grep registry | cut -d" " -f1 )); \
#                   do docker stop $i; docker rm --force $i;done' #'for i in $(docker ps -aq);do docker stop $i; docker rm --force $i;done'
# alias drmi_all='for i in $(docker images -q);do docker rmi $i;done'
# 


function mkd {
  dir="$1"
  if ! [ -d "${dir}" ]; then
    mkdir -p "${dir}"
  fi
}

function run_container {
# reference docker create command: https://docs.docker.com/engine/reference/commandline/create/
# reference docker run command: https://docs.docker.com/engine/reference/run
# unfortunatelly few options usefull for run command are described in reference for docker run
# answer are on the end of file
  # name of container
  cont='centos'

  # run container in background - otherwise it will occupy your terminal
  background=''

  # you need two options to make container interactive with correctly configured tty
  interactive_control=''
  # find centos image (cmd docker search)
  image='ubuntu:latest'

  host_dir='/media/host_side'
  # map volume on host side it will be path in ${host_dir} in container in /tmp
  volume=""

  # publish port 5050/tcp from container to ip 127.0.0.1 port 8080 
  ports=''

  # don't touch for now :)
  cmd='/usr/bin/sleep infinity'

  # what should happen in case of container failure
  restart_policy='always'

  # good to set workdir to path which user will need probably 
  workdir='/opt'

  # usually you don't want this - you remove security restrictions
  # check --cap-add and --cap-drop, if you are not familiar with kernel capabilities, skip this
  privileged='' #--privileged'

  # check environment of container
  env='TEST="Hello world"'

  mkd "${host_dir}"

	docker run \
    ${background} \
    ${interactive_control}
    --restart ${restart_policy} \
    --workdir ${workdir} \
     ${volume} \
    ${ports} \
    --name ${cont} \
    --env "${env}" \
    ${privileged} \
    ${image}  \
    ${cmd}

}

run_container $@

#background=--detach
#volume="--volume ${host_dir}:/tmp"
#ports='--publish 127.0.0.1:8080:5050'
#interactive_control='--interactive --tty'
